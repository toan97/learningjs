let products = [
    {
        id: 1, name: 'Đồng hồ nam Lobinni No', price: 700000, manufacturer: 'Lobinni No',
        image: 'https://donghobaothanh.vn/uploads/lobinni_no-1023lt-8_avt.png',
    },
    {
        id: 2, name: 'Đồng hồ nam MVW MS001-02', price: 500000, manufacturer: 'MVW',
        image: 'https://cdn.tgdd.vn/Products/Images/7264/210308/mvw-ms001-02-nam-vang-2-org.jpg'
    },
    {
        id: 3, name: 'Đồng hồ Nam Citizen cơ', price: 1000000, manufacturer: 'Citizen',
        image: 'https://cdn.tgdd.vn/Products/Images/7264/200941/citizen-nh8353-00h-xam-glr-1-org.jpg'
    },
    {
        id: 4, name: 'Đồng hồ Nam Tommy Hilfiger', price: 1000000, manufacturer: 'Hilfiger',
        image: 'https://cdn.tgdd.vn/Products/Images/7264/210308/mvw-ms001-02-nam-vang-2-org.jpg'
    },
    {
        id: 5, name: 'Đồng hồ Nam Citizen', price: 1000000, manufacturer: 'Citizen',
        image: 'https://donghobaothanh.vn/uploads/lobinni_no-1023lt-8_avt.png',
    },
    {
        id: 6, name: 'Đồng hồ nam MVW MS001-01', price: 1000000, manufacturer: 'MVW',
        image: 'https://cdn.tgdd.vn/Products/Images/7264/252017/tommy-1710417-nam-1.jpg',
    }
]
let productIdTpm

function actionPopup(isPopup) {
    let formAddProDuct = document.querySelector('#wrapper-add-product')
    if (isPopup) {
        formAddProDuct.classList.add('active')
    } else {
        formAddProDuct.classList.remove('active')
    }
}
function listProduct() {
    let productsView = document.getElementById('products')
    const htmlTable =
        `<table class="table table-hover" id="table-products">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col">Tên</th>
                <th scope="col">Giá</th>
                <th scope="col">Hãng</th>
                <th scope="col">Ảnh</th>
                <th scope="col" class="action text-center" colspan="2">
                    <a href="javascript:;" class="btn btn-success" onclick="openPopup()">Thêm mới</a>
                </th>
                </tr>
            </thead>
            <tbody> :TABLE_CONTENT </tbody>`
    // console.log(products)
    const htmlContent = products.map((product, index) => {

        return `<tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.price}</td>
                    <td>${product.manufacturer}</td>
                    <td><img src="${product.image}"/></td>
                    <td>
                        <button class="btn btn-primary" onclick="clickUpdateProduct(${index})">Cập nhật</button>
                    </td>
                    <td>
                        <button class="btn btn-danger" onclick="deleteProduct(${index})">Xóa</button>
                    </td>
                </tr>`
    }).join(' ')
    productsView.innerHTML = htmlTable.replace(':TABLE_CONTENT', htmlContent)
}

listProduct()
function closePopup() {
    actionPopup(false)
}

let editMode = false
function enableEditMode() {
    editMode = true
}

function disableEditMode() {
    editMode = false
}

function clickUpdateProduct(productIndex) {
    openPopup()
    productIdTpm = productIndex
    let product = products[productIndex]
    let id = document.getElementById('idProduct')
    let name = document.getElementById('nameProduct')
    let price = document.getElementById('priceProduct')
    let image = document.getElementById('imageProduct')
    let manufacturer = document.getElementById('manufacturerProduct')
    id.value = product.id
    name.value = product.name
    price.value = product.price
    image.value = product.image
    manufacturer.value = product.manufacturer
    enableEditMode()
    let setHtmlBtn = document.getElementById('btn-update')
    setHtmlBtn.innerHTML = 'Cập nhật'
}

function updateProduct() {
    let id = document.getElementById('idProduct').value
    let name = document.getElementById('nameProduct').value
    let price = document.getElementById('priceProduct').value
    let image = document.getElementById('imageProduct').value
    let manufacturer = document.getElementById('manufacturerProduct').value
    products[productIdTpm] = {
        id,
        name,
        price,
        image,
        manufacturer
    }
    listProduct()
    disableEditMode()
    closePopup()
}

function deleteProduct(productIndex) {
    if (confirm('Bạn có chắc chắn muốn xóa?')) {
        products.splice(productIndex, 1)
        listProduct()
    }
}

function openPopup() {
    actionPopup(true)
}

function clickCreateProduct() {
    if (editMode == true) {
        updateProduct()
    } else {
        const id = document.getElementById('idProduct').value
        const name = document.getElementById('nameProduct').value
        const price = document.getElementById('priceProduct').value
        const image = document.getElementById('imageProduct').value
        const manufacturer = document.getElementById('manufacturerProduct').value
        const product = {
            id: id,
            name: name,
            price: price,
            image: image,
            manufacturer: manufacturer,
        }
        addProduct(product)
    }
}
function addProduct(product) {
    products.push(product)
    listProduct()
    closePopup()
}

